import { useState } from "react";
import tshirt from "./tshirt.png";

const Products = ({ title, type, selected }) => {
  const [radioSelected, setRadioSelected] = useState(selected);

  const radioChanged = (event) => {
    setRadioSelected(event.target.checked);
  };

  const [count, setCount] = useState(0);
  return (
    <div className="products">
      <input
        className="products__Button"
        type="radio"
        name="check"
        value="first"
        checked={radioSelected}
        onChange={radioChanged}
      />
      <img className="product__Photo" src={tshirt} alt="tshirt" />
      <h5 className="title"> T-shirt</h5>
      <h5 className="price"> 20$</h5>
      <button
        onClick={() => setCount(count + 1)}
        className="product__add"
        type="button"
      >
        buy {count}
      </button>
    </div>
  );
};

export default Products;

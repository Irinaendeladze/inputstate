import { useState } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const DateValue = () => {
  const [dateValue, setDateValue] = useState(null);
  return (
    <div className="input__dates">
      <h5 className="datePiker">Date Picker</h5>
      <DatePicker selected={dateValue} onChange={(d) => setDateValue(d)} />
      <button className="resetButton" onClick={() => setDateValue(null)}>
        Reset
      </button>
    </div>
  );
};

export default DateValue;

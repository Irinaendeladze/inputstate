import "./App.css";
import Service from "./Service";
import AddProducts from "./AddProducts";
import Dates from "./Dates";

function App() {
  return (
    <div className="App">
      <div className="inputValue">
        <Service />
        <AddProducts />
        <Dates />
        <input type="search" />
      </div>
    </div>
  );
}

export default App;

import { useState } from "react";

const EachService = ({ title, type, selected }) => {
  const [inputSelected, setInputSelected] = useState(selected);

  const checkboxChanged = (event) => {
    setInputSelected(event.target.checked);
  };

  return (
    <div className="firstInput">
      <input
        className="checkbox__check"
        type="checkbox"
        name="check"
        checked={inputSelected}
        onChange={checkboxChanged}
      />
    </div>
  );
};

export default EachService;
